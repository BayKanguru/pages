const COPY_BUTTON_DEFAULT = "Copy";
const COPY_BUTTON_CLICK   = "Copied!";

function CopyButton(code_block, others) {
  let copy_button = document.createElement("input");

  copy_button.setAttribute("class", "copy-button");
  copy_button.setAttribute("type", "button");
  copy_button.setAttribute("value", COPY_BUTTON_DEFAULT);
  copy_button.setAttribute("aria-label", "Copy this code to clipboard.");

  copy_button.addEventListener("click", () => {
    navigator.clipboard.writeText(this.block.textContent);
    others.forEach((o) => o.reset());
    this.button.setAttribute("value", COPY_BUTTON_CLICK);
  });

  this.button = copy_button;
  this.block = code_block;
}

CopyButton.prototype.reset = function () {
  this.button.setAttribute("value", COPY_BUTTON_DEFAULT);
};

const code_blocks = document.querySelectorAll(".content pre[data-lang]");
let copy_buttons = [];

code_blocks.forEach((block) => {
  const button = new CopyButton(block, copy_buttons);
  copy_buttons.push(button);
  block.append(button.button);
});
