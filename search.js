// inject the search bar into view
// this done so that if javascript is disabled, no search bar is shown.
const header = document.querySelector(".primary-header");
const form_content = '<form autocomplete="off" class="search-form" novalidate="" role="search"><label class="hidden" for="search">Search for:</label><input enterkeyhint="search" id="search" type="search"><button><span class="hidden">Search</span> <svg viewBox="0 0 100 100" aria-hidden="true" focusable="false" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg"><path d="M73.37 62.76a39 39 0 10-10.61 10.61L85.2 95.8a7.5 7.5 0 0010.6-10.6zM41 65a24 24 0 1124-24 24 24 0 01-24 24z"></path></svg></button></form>';
let search_results = document.createElement("ul");
search_results.setAttribute("class", "search-results");
search_results.setAttribute("role", "list");

header.insertAdjacentHTML('beforeend', form_content);
header.append(search_results);

// this selects `form_content`
const input_form = document.querySelector(".search-form");

const MAX_RESULTS = 10;
const OPTIONS = {
  bool: "AND",
  fields: {
    title: {boost: 2},
    body: {boost: 1},
  }
};

// Taken from mdbook
// The strategy is as follows:
// First, assign a value to each word in the document:
//  Words that correspond to search terms (stemmer aware): 40
//  Normal words: 2
//  First word in a sentence: 8
// Then use a sliding window with a constant number of words and count the
// sum of the values of the words within the window. Then use the window that got the
// maximum sum. If there are multiple maximas, then get the last one.
// Enclose the terms in <b>.
/// Taken from https://github.com/getzola/zola/blob/d3fec80de1e5cb929ca573c8bbce69a93ade3127/docs/static/search.js
function makeTeaser(body, terms) {
  const TERM_WEIGHT = 40;
  const NORMAL_WORD_WEIGHT = 2;
  const FIRST_WORD_WEIGHT = 8;
  const TEASER_MAX_WORDS = 30;

  let stemmedTerms = terms.map(function (w) {
    return elasticlunr.stemmer(w.toLowerCase());
  });
  let termFound = false;
  let index = 0;
  let weighted = []; // contains elements of ["word", weight, index_in_document]

  // split in sentences, then words
  let sentences = body.toLowerCase().split(". ");

  for (const i in sentences) {
    let words = sentences[i].split(" ");
    let value = FIRST_WORD_WEIGHT;

    for (const j in words) {
      let word = words[j];

      if (word.length > 0) {
        for (const k in stemmedTerms) {
          if (elasticlunr.stemmer(word).startsWith(stemmedTerms[k])) {
            value = TERM_WEIGHT;
            termFound = true;
          }
        }
        weighted.push([word, value, index]);
        value = NORMAL_WORD_WEIGHT;
      }

      index += word.length;
      index += 1;  // ' ' or '.' if last word in sentence
    }

    index += 1;  // because we split at a two-char boundary '. '
  }

  if (weighted.length === 0) {
    return body;
  }

  let windowWeights = [];
  let windowSize = Math.min(weighted.length, TEASER_MAX_WORDS);
  // We add a window with all the weights first
  let curSum = 0;
  for (let i = 0; i < windowSize; i++) {
    curSum += weighted[i][1];
  }
  windowWeights.push(curSum);

  for (let i = 0; i < weighted.length - windowSize; i++) {
    curSum -= weighted[i][1];
    curSum += weighted[i + windowSize][1];
    windowWeights.push(curSum);
  }

  // If we didn't find the term, just pick the first window
  let maxSumIndex = 0;
  if (termFound) {
    let maxFound = 0;
    // backwards
    for (let i = windowWeights.length - 1; i >= 0; i--) {
      if (windowWeights[i] > maxFound) {
        maxFound = windowWeights[i];
        maxSumIndex = i;
      }
    }
  }

  let teaser = [];
  let startIndex = weighted[maxSumIndex][2];
  for (let i = maxSumIndex; i < maxSumIndex + windowSize; i++) {
    let word = weighted[i];
    if (startIndex < word[2]) {
      // missing text from index to start of `word`
      teaser.push(body.substring(startIndex, word[2]));
      startIndex = word[2];
    }

    // add <em/> around search terms
    if (word[1] === TERM_WEIGHT) {
      teaser.push("<b>");
    }
    startIndex = word[2] + word[0].length;
    teaser.push(body.substring(word[2], startIndex));

    if (word[1] === TERM_WEIGHT) {
      teaser.push("</b>");
    }
  }
  teaser.push("…");
  return teaser.join("");
}

async function init_index(path) {
  if (index === undefined) {
    index = fetch(path)
      .then(
        async function(response) {
          return await elasticlunr.Index.load(await response.json());
      }
    );
    res = await index;
    return res;
  } else {
    return index;
  }
}

/// Generates a search result like this:
/// <li class="card">
///   <a href="{{ perma }}">
///     <h3>{{ title }}</h3>
///     <p class="card-description">{{ body }}</p>
///   </a>
/// </li>
function render_result() {
    let result = document.createElement("li");
    result.setAttribute("class", "search-result card");
    let anchor = document.createElement("a");
    anchor.setAttribute("href", this.perma);

    let res_title = document.createElement("h3");
    res_title.innerHTML = this.title;

    res_body = document.createElement("p");
    res_body.setAttribute("class", "card-description");
    res_body.innerHTML = this.body;

    anchor.append(res_title);
    anchor.append(res_body);
    result.append(anchor);

    return result;
}

function clear_results() {
  search_results.removeAttribute("display-results");
  while (search_results.firstChild) {
    search_results.removeChild(search_results.firstChild);
  }
}

let last_query = "";
let index;

input_form.addEventListener("submit", async (event) => {
  event.preventDefault();

  let query = input_form.children.namedItem("search").value;

  if (query === ""){
    clear_results();
    last_query = "";
    return;
  }
  if (query === last_query) {
    return;
  }
  last_query = query;

  index = await init_index("/search_index.en.json");

  let raw_results = index
                      .search(query, OPTIONS)
                      .slice(0, MAX_RESULTS);

  let transform = x => {
    return makeTeaser(x.doc.body, query.split(" "));
  };
  let results = raw_results.map(x => {
    return {
      title: x.doc.title,
      body: transform(x),
      perma: x.doc.id,
      render: render_result,
    }
  });
  clear_results();
  search_results.setAttribute("display-results", true);

  for (const result of results) {
    search_results.append(result.render());
  }
})
